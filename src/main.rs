extern crate failure;
extern crate orca;

use failure::Error;
use orca::{App, Sort, SortTime};

fn main() {
    match run() {
        Ok(_) => println!("ran"),
        Err(_) => println!("errored"),
    }
}

fn run() -> Result<(), Error> {
    let reddit = App::new("aminal", "0.0.1", "author")?;

    let posts = &reddit.get_posts("aww", Sort::Top(SortTime::Day))?;
    let posts = &posts["data"]["children"];

    // TODO: Instead of starting from 3, explicitly reject stickied posts
    for i in 3..7 {
        match posts[i]["data"]["permalink"].as_str() {
            Some(s) => {
                let link = format!("https://reddit.com{}", s);
                println!("{}", link);
            }
            None => println!("permalink not found"),
        }
    }

    Ok(())
}
